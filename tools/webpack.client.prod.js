const path = require('path');
const webpack = require('webpack');
const AssetsPlugin = require('assets-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const autoprefixer = require('autoprefixer')
const CONFIG = require('./webpack.base');
const CLIENT_ENTRY = CONFIG.CLIENT_ENTRY;
const CLIENT_OUTPUT = CONFIG.CLIENT_OUTPUT;
const PUBLIC_PATH = CONFIG.PUBLIC_PATH;
const COMMON_PATH = CONFIG.COMMON_PATH;
const sassLoaders = [
	'css-loader',
	'postcss-loader',
	'sass-loader?sourceMap!sass?sourceMap&includePaths[]=' + path.resolve(COMMON_PATH)
]


module.exports = {
	devtool: false,
	entry: {
		main: [CLIENT_ENTRY],
		vendor: [
			'react',
			'react-dom',
			'react-router',
			'redux',
			'react-redux',
			'aphrodite'
		],
	},
	output: {
		filename: '[name]_[chunkhash].js',
		chunkFilename: '[name]_[chunkhash].js',
		publicPath: PUBLIC_PATH,
		path: CLIENT_OUTPUT
	},
	plugins: [
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.AggressiveMergingPlugin(),
		new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js', 2),
		new AssetsPlugin({filename: 'assets.json'}),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				unused: true,
				dead_code: true,
				warnings: false,
				screw_ie8: true
			}
		}),
		new webpack.NoErrorsPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('production'),
			'__DEV__': false
		}),
		new ExtractTextPlugin('[name].css'),
	],
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel',
				query: {
					cacheDirectory: true,
					presets: ["es2015", "react", "stage-0"]
				},
				exclude: /(node_modules)/
			},
			{
				test: /\.css$/,
				loaders: ['style', 'css']
			},
			{
				test: /\.scss$/,
				exclude: /(node_modules)/,
				loader: ExtractTextPlugin.extract('style-loader', sassLoaders.join('!'))
			}
		]
	},
	postcss: [
		autoprefixer({
			browsers: ['last 2 versions']
		})
	],
}
