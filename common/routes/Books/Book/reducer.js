import * as types from './constants'
import {Stack, Map} from 'immutable'

const initialState = Map({
	lastFetched: null,
	isLoading: false,
	error: null,
	data: Stack()
})

export default function currentBook(state = initialState, action) {
	switch (action.type) {
		case types.LOAD_BOOK_REQUEST:
			return state.withMutations(book => {
				book.set('isLoading', true)
				.set('error', null)
			})
		case types.LOAD_BOOK_SUCCESS:
			return state.withMutations(book => {
				book.set('data', action.payload)
				.set('lastFetched', action.meta.lastFetched)
				.set('isLoading', false)
			})
		case types.LOAD_BOOK_FAILURE:
			return state.set('error', action.payload)
		default:
			return state
	}
}

// Example of a co-located selector
// return immutable map as plain JS object
export const selectCurrentBook = state => state.currentBook.toJS()
