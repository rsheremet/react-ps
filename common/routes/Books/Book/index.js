if (typeof require.ensure !== 'function') require.ensure = (d, c) => c(require)
import {ROOT_STORE_PATH} from './constants'
import {injectAsyncReducer} from '../../../store'

export default function createRoutes(store) {
	return {
		path: 'book/:id',
		getComponents (location, cb) {
			require.ensure([
				'./containers/BookPage',
				'./reducer'
			], (require) => {
				let BookPage = require('./containers/BookPage').default
				let bookReducer = require('./reducer').default
				injectAsyncReducer(store, 'currentBook', bookReducer)
				cb(null, BookPage)
			})
		}
	}
}
