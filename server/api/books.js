import {Router} from 'express'
const router = new Router();
const request = require('request');

const API = {
	apiHost: 'http://it-ebooks-api.info/v1',
	searchByText: '/search/:query',
	searchByTextWithPage: '/search/:query/page/:page',
	getBookById: '/book/:id'
};

function getUrl(name, external) {
	if (API[name]) {
		if (external) {
			return API.apiHost + API[name];
		}
		return API[name];
	} else {
		return '/';
	}
}


function searchByText(req, res) {

	var uri = getUrl('searchByText', true).replace(':query', req.params.query);

	function reqCb(error, response, body) {

		if (response.statusCode == 200) {
			try {
				var bodySource = JSON.parse(body);
				res.status(200).json(bodySource);
			} catch (e) {
				console.error(e);
				res.status(404).json();
			}

		} else {
			console.log('error: ' + response.statusCode)
			console.log(body)
		}
	}

	request({uri: uri, method: 'GET'}, reqCb);
}

function searchByTextWithPage(req, res) {

	var uri = getUrl('searchByTextWithPage', true)
	.replace(':query', req.params.query)
	.replace(':page', req.params.page);

	function reqCb(error, response, body) {
		if (response.statusCode == 200) {
			try {
				var bodySource = JSON.parse(body);
				res.status(200).json(bodySource);
			} catch (e) {
				console.error(e);
				res.status(404).json();
			}
		} else {
			console.log('error: ' + response.statusCode)
			console.log(body)
		}
	}

	request({
		uri: uri,
		method: 'GET'
	}, reqCb);
}

function getBookById(req, res) {
	var uri = getUrl('getBookById', true).replace(':id', req.params.id);
	console.log('getBookById:uri', uri);
	function reqCb(error, response, body) {

		if (response.statusCode == 200) {
			try {
				var bodySource = JSON.parse(body);
				res.status(200).json(bodySource);
			} catch (e) {
				console.error(e);
			}

		} else {
			console.log('error: ' + response.statusCode)
			console.log(body)
		}
	}

	request({uri: uri}, reqCb);
}


router.get(getUrl('searchByText'), searchByText);

router.get(getUrl('searchByTextWithPage'), searchByTextWithPage);

router.get(getUrl('getBookById'), getBookById);

module.exports = router;
