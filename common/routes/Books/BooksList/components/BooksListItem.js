import React from 'react'
import {Link} from 'react-router'
import {StyleSheet, css} from 'aphrodite'

const BooksListItem = ({book}) => (
	<div className={css(styles.root)}>
		<div className={css(styles.left)}>
			<img width="200" height="250" src={book.Image}/>
			<div>ISBN: {book.isbn}</div>
		</div>
		<div className={css(styles.right)}>
			<h3><Link to={`/book/${book.ID}`} className={css(styles.title)}> {book.Title} </Link></h3>
			<h4 className={css(styles.subtitle)}>{book.SubTitle}</h4>
			<p>{book.Description}</p>
		</div>
	</div>

)

const styles = StyleSheet.create({
	left: {
		display: 'block',
		float: 'left',
		padding: '5px 0',
		width: '25%'
	},
	right: {
		display: 'block',
		float: 'right',
		padding: '5px 0',
		width: '70%'
	},
	root: {
		display: 'block',
		margin: '0 auto 1.5rem',
		maxWidth: 960,
		clear: 'both'
	},
	title: {
		display: 'block',
		fontSize: 28,
		textDecoration: 'none',
		lineHeight: '1.2',
		margin: '10px 0 1.5rem',
		color: '#666',
		transition: '.3s opacity ease',
		':hover': {
			opacity: 0.5
		}
	},
	subtitle: {
		fontSize: 18,
		lineHeight: '1.2',
		color: '#999'
	}
})

export default BooksListItem
