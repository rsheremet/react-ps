import * as types from './constants'
import update from 'react/lib/update'
import Immutable, {Stack, Map} from 'immutable'

const initialState = Map({
	data: Stack(),
	lastFetched: null,
	isLoading: false,
	error: null
})

export default function books(state = initialState, action) {
	switch (action.type) {
		case types.LOAD_BOOKS_REQUEST:
			return state.withMutations(books => {
				books.set('isLoading', true)
				.set('error', null)
			})
		case types.LOAD_BOOKS_SUCCESS:
			return state.withMutations(books => {
				books.set('data', Immutable.fromJS(action.payload))
				.set('lastFetched', action.meta.lastFetched)
				.set('isLoading', false)
			})
		case types.LOAD_BOOKS_FAILURE:
			return state.set('error', action.payload)
		default:
			return state
	}
}

// Example of a co-located selector
export const selectBooks = state => state.books.toJS()
