import {provideHooks} from 'redial'
import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {loadBook} from '../actions'
import {StyleSheet, css} from 'aphrodite'
import Helmet from 'react-helmet'
import NotFound from '../../../../components/NotFound'
import {selectCurrentBook} from '../reducer'


const redial = {
	fetch: ({dispatch, params: {id}}) => dispatch(loadBook(id))
}

const mapStateToProps = state => selectCurrentBook(state)

const BookPage = ({data, isLoading, error}) => {
	if (!error) {
		return (
			<div className="books-details">
				<Helmet title={data.Title}/>
				{isLoading &&
				<div>
					<h2 className={css(styles.loading)}>Loading....</h2>
				</div>}
				{!isLoading &&
				<div className={css(styles.root)}>
					<div className={css(styles.left)}>
						<img width="200" height="250" src={data.Image}/>
						<div className="padding-y-5"><b>Publisher:</b> {data.Publisher}</div>
						<div className="padding-y-5"><b>Author:</b> {data.Author}</div>
						<div className="padding-y-5"><b>Year:</b> {data.Year}</div>
						<div className="padding-y-5"><b>Pages:</b> {data.Page}</div>
						<div className="padding-y-5"><b>ISBN:</b> {data.ISBN}</div>
						<div className="padding-y-5"><a href={data.Download} target="_blank">[Download]</a></div>
					</div>
					<div className={css(styles.right)}>
						<h3 className={css(styles.title)}> {data.Title}</h3>
						<h4 className={css(styles.subtitle)}>{data.SubTitle}</h4>
						<p>{data.Description}</p>
					</div>
				</div>}
			</div>
		)
	} else {
		// maybe check for different types of errors and display appropriately
		return <NotFound />
	}
}

BookPage.propTypes = {
	title: PropTypes.string,
	content: PropTypes.string,
	isLoading: PropTypes.bool,
	error: PropTypes.object
}

const styles = StyleSheet.create({
	left: {
		display: 'block',
		float: 'left',
		padding: '5px 0',
		width: '25%'
	},
	right: {
		display: 'block',
		float: 'right',
		padding: '5px 0',
		width: '70%'
	},
	root: {
		display: 'table',
		margin: '0 auto 1.5rem',
		maxWidth: 960
	},
	content: {
		fontSize: '1rem',
		lineHeight: '1.5',
		margin: '1rem 0',
		color: '#555'
	},
	title: {
		fontSize: 28,
		margin: '0 auto 1.5rem',
		color: '#000'
	},
	loading: {
		fontSize: 28,
		margin: '0 auto 1.5rem',
		color: '#b7b7b7'
	}
})

export default provideHooks(redial)(connect(mapStateToProps)(BookPage))
