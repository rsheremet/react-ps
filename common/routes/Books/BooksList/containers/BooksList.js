import {provideHooks} from 'redial'
import React, {PropTypes} from 'react'
import {loadBooks} from '../actions'
import {connect} from 'react-redux'
import BooksListItem from '../components/BooksListItem'
import {StyleSheet, css} from 'aphrodite'
import Helmet from 'react-helmet'
import {selectBooks} from '../reducer'

const redial = {
	fetch: ({dispatch, params: {query}}) => dispatch(loadBooks(query))
}

const mapStateToProps = state => ({
	books: selectBooks(state)
})

const BooksListPage = ({books}) => (
	<div className={css(styles.root)}>
		<Helmet title='Books'/>
		{books.isLoading &&
		<div>
			<h2 className={css(styles.title)}>Loading....</h2>
		</div>}

		{!books.isLoading && books.data.hasOwnProperty('Books') && books.data.Books.map((book, i) =>
			<div key={i} className={css(styles.item)}>
				{(()=> {
					if (i > 0) {
						return (<div className={css(styles.separator)}></div>)
					}
				})()}
				<BooksListItem book={book}/>
			</div>
		)}

	</div>
)

BooksListPage.PropTypes = {
	books: PropTypes.object.isRequired
}

const styles = StyleSheet.create({
	root: {
		display: 'table',
		maxWidth: 960
	},
	title: {
		fontSize: 28,
		margin: '5px auto 1.5rem',
		color: '#b7b7b7'
	},
	item: {
		display: 'table'
	},
	separator: {
		width: '100%',
		height: 10,
		margin: '5px 0',
		borderBottom: '1px solid #dedede',
		display: 'table'
	}
})

export default provideHooks(redial)(connect(mapStateToProps)(BooksListPage))
