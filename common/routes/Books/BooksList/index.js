if (typeof require.ensure !== 'function') require.ensure = (d, c) => c(require)
import {injectAsyncReducer} from '../../../store'

export default function createRoutes(store) {
	return {
		path: 'books/search/:query',
		getComponents (location, cb) {
			require.ensure([
				'./containers/BooksList',
				'./reducer'
			], (require) => {
				let BooksList = require('./containers/BooksList').default
				let bookReducer = require('./reducer').default
				injectAsyncReducer(store, 'books', bookReducer)
				cb(null, BooksList)
			})
		}
	}
}
