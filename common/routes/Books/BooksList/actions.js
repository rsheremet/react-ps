import {LOAD_BOOKS_REQUEST, LOAD_BOOKS_SUCCESS, LOAD_BOOKS_FAILURE} from './constants'

export function loadBooks(query) {

	return (dispatch, getState, {axios}) => {
		const {protocol, host} = getState().sourceRequest
		dispatch({type: LOAD_BOOKS_REQUEST})
		return axios.get(`${protocol}://${host}/api/v0/books/search/${query || 'JavaScript'}`)
		.then(res => {
			dispatch({
				type: LOAD_BOOKS_SUCCESS,
				payload: res.data,
				meta: {
					lastFetched: Date.now()
				}
			})
		})
		.catch(error => {
			console.error(`Error in reducer that handles ${LOAD_BOOKS_SUCCESS}: `, error)
			dispatch({
				type: LOAD_BOOKS_FAILURE,
				payload: error,
				error: true
			})
		})
	}
}
