const path = require('path')
const webpack = require('webpack')
const CONFIG = require('./webpack.base')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const autoprefixer = require('autoprefixer')

const {CLIENT_ENTRY, CLIENT_OUTPUT, PUBLIC_PATH, COMMON_PATH} = CONFIG

const sassLoaders = [
	'css-loader',
	'postcss-loader',
	'sass-loader?sourceMap!sass?sourceMap&includePaths[]=' + path.resolve(COMMON_PATH)
]

module.exports = {
	devtool: 'cheap-module-eval-source-map',
	entry: {
		main: [
			'webpack/hot/only-dev-server',
			'webpack-hot-middleware/client',
			CLIENT_ENTRY
		],
		vendor: [
			'react',
			'react-dom',
			'react-router',
			'redux',
			'react-redux',
			'aphrodite'
		]
	},
	output: {
		filename: '[name].js',
		chunkFilename: '[name].chunk.js',
		publicPath: PUBLIC_PATH,
		path: CLIENT_OUTPUT
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js', 2),
		new webpack.NoErrorsPlugin(),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('development'),
			'__DEV__': true
		}),
		new ExtractTextPlugin('[name].css')
	],
	module: {
		preLoaders: [
			{
				// set up standard-loader as a preloader
				test: /\.jsx?$/,
				loader: 'standard',
				exclude: /(node_modules)/
			}
		],
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel',
				exclude: /(node_modules|server)/,
				query: {
					cacheDirectory: true,
					presets: ["es2015", "react", "stage-0"]
				}
			},
			{
				test: /\.scss$/,
				exclude: /(node_modules)/,
				loader: ExtractTextPlugin.extract('style-loader', sassLoaders.join('!'))
			}
		]
	},
	postcss: [
		autoprefixer({
			browsers: ['last 2 versions']
		})
	],
	standard: {
		// config options to be passed through to standard e.g.
		parser: 'babel-eslint'
	},
	eslint: {
		//failOnWarning: true,
		emitWarning: false,
		failOnError: true,
	}
}
